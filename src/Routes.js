import React from "react";
import { Route, Switch } from "react-router-dom";
import { HOME_URL, LOGIN_URL, DASHBOARD_URL } from "./containers/urls";
import { ProtectedRoute } from "./components/ProtectedRoute";

import Homepage from "./containers/Homepage";
import Login from "./containers/Login";
import Dashboard from "./containers/Dashboard";

export default function AppRouter() {
  return (
    <Switch>
      <Route exact path={HOME_URL} component={Homepage} />
      <Route exact path={LOGIN_URL} component={Login} />
      <ProtectedRoute exact path={DASHBOARD_URL} component={Dashboard} />
      <Route path="*" component={() => "404 Not Found"} />
    </Switch>
  );
}
