import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { BrowserRouter } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";
// Importing the Bootstrap CSS
import "bootstrap/dist/css/bootstrap.min.css";
import AppRouter from "./Routes";
import { AppContext } from "./lib/contextLib";
import { Amplify, Auth } from "aws-amplify";
import awsmobile from "./aws-exports";
import { Link } from "react-router-dom";
import Autorizado from "./components/Autorizado";
import { HOME_URL, LOGIN_URL, DASHBOARD_URL } from "./containers/urls";
import { useHistory } from "react-router-dom";
import { Navbar, Container, Nav } from "react-bootstrap";

Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: awsmobile.aws_cognito_region,
    userPoolId: awsmobile.aws_user_pools_id,
    userPoolWebClientId: awsmobile.aws_user_pools_web_client_id,
  },
});

function App() {
  let history = useHistory();
  const [isAuthenticated, userHasAuthenticated] = useState(false);
  const [isAuthenticating, setIsAuthenticating] = useState(true);

  useEffect(() => {
    onLoad();
  }, []);

  async function onLoad() {
    try {
      await Auth.currentSession();
      userHasAuthenticated(true);
    } catch (e) {
      console.log("Usuario No Logeado");
      if (e !== "No current user") {
        alert(e);
      }
    }

    setIsAuthenticating(false);
  }

  async function handleLogout() {
    await Auth.signOut();
    userHasAuthenticated(false);
    Autorizado.logout(() => {
      history.push(LOGIN_URL);
    });
  }

  return (
    !isAuthenticating && (
      <div className="App">
        <AppContext.Provider value={{ isAuthenticated, userHasAuthenticated }}>
          <nav>
            {isAuthenticated ? (
              <>
                <Navbar bg="light" expand="lg">
                  <Container>
                    <Navbar.Brand href={HOME_URL}>
                      Aplicación Integrada a Cognito
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                      <Nav className="me-auto">
                        <Link className="nav-link" to={HOME_URL}>
                          Home
                        </Link>

                        <Link className="nav-link" to={DASHBOARD_URL}>
                          Dashboard
                        </Link>
                      </Nav>
                      <Nav>
                        <Link
                          className="nav-link"
                          to={HOME_URL}
                          onClick={handleLogout}
                        >
                          Cerrar Sesion
                        </Link>
                      </Nav>
                    </Navbar.Collapse>
                  </Container>
                </Navbar>
              </>
            ) : (
              <Navbar bg="light" expand="lg">
                <Container>
                  <Navbar.Brand href={HOME_URL}>
                    Aplicación Integrada a Cognito
                  </Navbar.Brand>
                  <Navbar.Collapse id="basic-navbar-nav">
                    <Link className="btn btn-sm btn-secondary" to={LOGIN_URL}>
                      Iniciar Sesión
                    </Link>
                  </Navbar.Collapse>
                </Container>
              </Navbar>
            )}
          </nav>
          <AppRouter />
        </AppContext.Provider>
      </div>
    )
  );
}

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
