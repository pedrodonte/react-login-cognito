import React, { useState } from "react";
import { Auth } from "aws-amplify";
import Form from "react-bootstrap/Form";
import Autorizado from "../components/Autorizado";
import Button from "react-bootstrap/Button";
import { useHistory } from "react-router-dom";
import "./Login.css";
import { useAppContext } from "../lib/contextLib";
import { DASHBOARD_URL } from "./urls";

export default function Login() {
  const { userHasAuthenticated } = useAppContext();
  let history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [token, setToken] = useState("");
  const [mensaje, setMensaje] = useState("");

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  async function handleSubmit(event) {
    event.preventDefault();
    setMensaje("");
    try {
      await Auth.signIn(email, password);
      setMensaje("Logged in");
      userHasAuthenticated(true);
      Autorizado.login(email, password, email, () => {
        history.push(DASHBOARD_URL);
      });

      var data = await Auth.currentSession();
      console.log({ data });
      setToken(data.accessToken.jwtToken);
    } catch (e) {
      setMensaje(e.message);
      setToken("");
    }
  }

  return (
    <div className="Login">
      <Form onSubmit={handleSubmit}>
        <Form.Group size="sm" controlId="username">
          <Form.Label>UserName</Form.Label>
          <Form.Control
            autoFocus
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" size="sm" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        <Button block size="sm" type="submit" disabled={!validateForm()}>
          Login
        </Button>
      </Form>
      <hr />

      <Form>
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
          <Form.Label>Resultado del Login</Form.Label>
          <Form.Control type="text" value={mensaje} />
        </Form.Group>
        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
          <Form.Label>JWT Obtenido</Form.Label>
          <Form.Control as="textarea" rows={3} value={token} />
        </Form.Group>
      </Form>
    </div>
  );
}
