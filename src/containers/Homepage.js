import React from "react";
import logo from "../logo.svg";
import "../App.css";

export default function Homepage() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Página Inicial del Proyecto</p>
      </header>
    </div>
  );
}
