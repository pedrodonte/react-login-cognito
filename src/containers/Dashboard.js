import React, { useState, useEffect } from "react";

import { Auth } from "aws-amplify";
import { Row, Container, Col } from "react-bootstrap";

export default function Dashboard() {
  const [token, setToken] = useState("");

  useEffect(() => {
    onLoad();
  }, []);

  async function onLoad() {
    try {
      var data = await Auth.currentSession();
      console.log({ data });
      setToken(data.idToken.jwtToken);
    } catch (e) {
      console.log("Usuario No Logeado");
      if (e !== "No current user") {
        alert(e);
      }
    }
  }
  return (
    <Container>
      <Row>
        <Col>
          <h1>Sitio Privado</h1>
        </Col>
      </Row>
      <Row>
        <Col>
          <h3>Datos de Autenticación (JWT) </h3>
          {token}
        </Col>
      </Row>
    </Container>
  );
}
