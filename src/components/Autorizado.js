class Auth {
  constructor() {
    this.authenticated = false;
    this.username = "";
    this.fullname = "";
  }

  login(user, password, name, cb) {
    this.username = user;
    this.fullname = name;
    this.authenticated = true;
    localStorage.setItem("auth", user);
    localStorage.setItem("authname", name);

    cb();
  }

  logout(cb) {
    this.authenticated = false;
    this.username = "";
    localStorage.removeItem("auth");

    cb();
  }

  isAuthenticated() {
    if (localStorage.getItem("auth")) {
      this.authenticated = true;
    } else {
      this.authenticated = false;
    }
    return this.authenticated;
  }

  getUsername() {
    this.username = localStorage.getItem("auth");
    //this.fullname = localStorage.getItem('authname');
    return this.username;
  }
}

export default new Auth();
