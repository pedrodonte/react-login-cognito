import React from "react";
import { Route, Redirect } from "react-router-dom";
import Autorizado from "./Autorizado";
import { LOGIN_URL } from "../containers/urls";
export const ProtectedRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        console.log({ Autorizado });
        if (Autorizado.isAuthenticated()) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: LOGIN_URL,
                state: {
                  from: props.location,
                },
              }}
            />
          );
        }
      }}
    />
  );
};
